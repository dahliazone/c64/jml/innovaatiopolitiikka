.IFDEF _INCLUDE_MACROS_H
.ELSE
_INCLUDE_MACROS_H=1

.macro SetPointer pointer, address
	lda #<(address)
	sta pointer
	lda #>(address)
	sta pointer+1
.endmacro

;Copy exactly 1000 bytes of memory. Useful for copying color ram, while excluding sprite pointers.
.macro memcpy1000 dst, src
.scope
	ldx #250
prev:
.repeat 4, i
	lda src-1+250*i, x
	sta dst-1+250*i, x
.endrepeat
	dex
	bne prev
.endscope
.endmacro

slidew=8
slideh=13

slidex=28
slidey=4
;NOTE: Expects src pointer to be set to src of slide data!
.macro memcpyslide
	dst=$4000+slidey*40*8+slidex*8
	SetPointer bitmapdstptr, dst
	ldx #slideh
prevx:
	ldy #slidew*8-1
.scope
prevy:
	lda (bitmapsrcptr), y
	sta (bitmapdstptr), y
	dey
	bpl prevy
	inc bitmapdstptr+1
	dex
	bne prevx
.endscope
.endmacro

.macro AddToPtr ptr, value
.scope
	lda ptr
	clc
	adc #.lobyte(value)
	sta ptr
.if value<256
	bcc nocarry
	inc ptr+1
nocarry:
.else
	lda ptr+1
	adc #.hibyte(value)
	sta ptr+1
.endif
.endscope
.endmacro

.ENDIF
