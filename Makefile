SHELL := /bin/bash
CPU = 6502
#         Adds also local symbols into debug info
AS = ca65 -g
ASFLAGS = --cpu $(CPU)
LD = ld65
LDFLAGS = -m labels.txt -Ln symbols -C config.cfg -o $(OUTPUT)
C1541 = c1541
#C1541 = /Applications/VICE/bin/c1541
X64 = x64sc
#X64 = /Applications/VICE/bin/x64sc
#X64 = Work:Emulators/VICE/x64

OBJS = \
	init.o \
	bars.o \
	music.o \
	sipilaimg.o \
	demosequence.o \
	last.o

OUTPUT = innovaatiopoli.prg
DISKFILENAME = innovaatiopoli.d64
DISKNAME = sipila
ID = 19

all: d64

kannatuslaskin: kannatuslaskin.py
	./kannatuslaskin.py -f

sipila: $(OBJS)
	$(LD) $(LDFLAGS) $(OBJS)

d64: kannatuslaskin sipila
	$(C1541) -format $(DISKNAME),$(ID) d64 $(DISKFILENAME)
	$(C1541) -attach $(DISKFILENAME) -write innovaatiopoli.prg
	$(C1541) -attach $(DISKFILENAME) -list

run: d64
#              Load code defined labels into VICE's monitor. Also for example "d .init" work instead of just direct addresses
	$(X64) -moncommands symbols $(DISKFILENAME)

clean:
	rm -f *.o innovaatiopoli.d64 innovaatopoli.prg labels.txt symbols $(DISKFILENAME)

