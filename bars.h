.importzp BarHeight
.importzp BarSpeed
.importzp BarTarget

.import InitBarsFirst
.import InitBarColors
.import UpdateBars

.import InitBars, RunBars

.macro AddBars spd0, tgt0,  spd1, tgt1,  spd2, tgt2,  spd3, tgt3,  spd4, tgt4,  spd5, tgt5,  spd6, tgt6,  spd7, tgt7,  spd8, tgt8,  spd9, tgt9,  spda, tgta,  spdb, tgtb,  spdc, tgtc,  spdd, tgtd,  spde, tgte,  spdf, tgtf
.segment "BARSPD"
.byte spd0, spd1, spd2, spd3, spd4, spd5, spd6, spd7, spd8, spd9, spda, spdb, spdc, spdd, spde, spdf
.segment "BARTGT"
.byte tgt0, tgt1, tgt2, tgt3, tgt4, tgt5, tgt6, tgt7, tgt8, tgt9, tgta, tgtb, tgtc, tgtd, tgte, tgtf

.segment "EMPTY"
.endmacro
