.macpack cbm

.include "macros_h.s"
.include "colors.h"

.import __BARSPD_RUN__, __BARTGT_RUN__

.exportzp BarHeight
.exportzp BarSpeed
.exportzp BarTarget

.export InitBarsFirst
.export InitBarColors
.export UpdateBars

.export InitBars, RunBars

;Upper left corner, excluding party name.
BarX=5
BarY=5
ColorRam=$D800

BarScreen=$0400

ScreenW=40
TitleW=3

BarAddress=BarScreen+BarX+BarY*ScreenW

MaxDelay=5

space=$20

.macro ReduceResolution
.repeat 0
	lsr a
.endrepeat
.endmacro

.macro AddPartyTitles
.scope
	TitleAddress=BarScreen+BarX+BarY*ScreenW-TitleW
	SetPointer BarPtr, TitleAddress

	ldx #16
nextparty:
	ldy #TitleW-1
prevchar:
namesmc:
	lda PartyNames, y
	sta (BarPtr), y
	dey
	bpl prevchar
	lda BarPtr
	clc
	adc #ScreenW
	sta BarPtr
	bcc noc1
	inc BarPtr+1
	clc
noc1:
	lda namesmc+1
	adc #3 ; Party name len
	sta namesmc+1
	bcc noc2
	inc namesmc+2
noc2:
	dex
	bne nextparty
.endscope
.endmacro

.proc InitBarColors
	ColorPtr=ColorRam+(BarY+16)*ScreenW
	SetPointer BarPtr, ColorPtr

	ldx #16
prevcolrow:
	lda BarColor, x
	ldy #ScreenW-1 ; For now, color entire rows.
prevcolchar:
	sta (BarPtr), y
	dey
	bpl prevcolchar
	lda BarPtr
	sec
	sbc #ScreenW
	sta BarPtr
	bcs carryclear1
	dec BarPtr+1
carryclear1:
	dex
	bpl prevcolrow
	rts
.endproc

.proc InitBarsFirst
	lda #0
	sta NumBarsRunning
	jsr ResetBars
	lda #MaxDelay
	sta delay
	lda #1
	sta $D020
	sta $D021
	lda #0
	sta debugindex
	AddPartyTitles
	
	SetPointer BarPtr, BarAddress
	SetPointer BarHeight, BarHeightStart
	
	ldy #15
prevy:
	lda (BarTarget), y
	sta BarRunning, y
	beq zeroamount
	inc NumBarsRunning
zeroamount:
	lda (BarHeight), y
	;Init single bar.x
	ReduceResolution
	sta ATemp
	and #%00000011
	tax ; Partial last char Id. Can be empty.
	sty YTemp
	lda ATemp
	lsr a
	lsr a
	tay
	lda PartialChar, x
	sta (BarPtr), y
	cpy #0
	beq zero
	dey
	lda #$E0
prevseg:
	sta (BarPtr), y
	dey
	bpl prevseg
zero:
	;FIXME This seems to get somehow corrupted into $E0 $E0...
	ldy YTemp
	
	lda BarPtr
	clc
	adc #ScreenW
	sta BarPtr
	bcc carryclear2
	inc BarPtr+1
	
carryclear2:
	dey
	bpl prevy
	
	rts
.endproc

.proc UpdateBars
	dec delay
	beq zero
	rts
zero:
	lda #MaxDelay
	sta delay

	SetPointer BarPtr, BarAddress
	ldy #16-1
prevbar:
	lda BarRunning, y
	beq skipbar
	;Add current speed to bar height.
	lda (BarHeight), y
	clc
	adc (BarSpeed), y
	sta (BarHeight), y
	pha
	;NOTE: Might be easier (dec, $1234, x) if we copy this array elsewhere on init...
	;Update how much longer the bar should run.
	lda (BarTarget), y
	sec
	sbc #1
	sta (BarTarget), y
	bne notarget
	;Bar reached target. Length should be updated this frame & bar ignored after that.
	lda #0
	sta BarRunning, y
	dec NumBarsRunning
notarget:
	pla
	ReduceResolution
	sta ATemp
	and #%00000011
	tax ; Last char Id (from array)
	lda ATemp
	lsr a
	lsr a ; Amount of reverse spaces
	sta ATemp
	sty YTemp
	tay ; Bar length (excluding partial)
	
	iny ; Add trailing space. Always.
	lda #space
	sta (BarPtr), y
	;Add partial char Always. Can be empty (from array).
	dey
	lda PartialChar, x
	sta (BarPtr), y
	cpy #0
	beq zerofullchars
	dey ; Amount of full char IDs left to draw in bar. Can be 0.
	lda #$E0
prevseg:
	sta (BarPtr), y
	dey
	bpl prevseg
zerofullchars:
	ldy YTemp
skipbar:
	lda BarPtr
	clc
	adc #ScreenW
	sta BarPtr
	bcc carryclear2
	inc BarPtr+1
carryclear2:
	dey
	bpl prevbar
	lda NumBarsRunning
	rts
.endproc

.proc ResetBars
	;FIXME: Copy initial state?
	;NOTE: -16 makes next call to NextBars correct the values.
	SetPointer BarSpeed, __BARSPD_RUN__-16
	SetPointer BarTarget, __BARTGT_RUN__-16
	rts
.endproc

.proc NextBars
	;Move to the next arrays in memory.
	AddToPtr BarSpeed, 16
	AddToPtr BarTarget, 16
	;Update bar running info.
	lda #0
	sta NumBarsRunning
	ldy #15
prevy:
	lda (BarTarget), y
	sta BarRunning, y
	beq zeroamount
	inc NumBarsRunning
zeroamount:
	dey
	bpl prevy
	rts
.endproc

.macro SelectCharMode
	lda #1
	sta $D021
	lda #%00000011 ; Default VIC block. For text effects and stuff.
	sta $DD00
	lda #%00011011 ; Select char mode.
	sta $D011
	lda #%00010101 ; Default char mode
	sta $D018
	lda #%11001000 ; Hires mode
	sta $D016
.endmacro

.proc InitBars
	jsr NextBars
	jsr InitBarColors
	SelectCharMode
	rts
.endproc

.proc RunBars
;.repeat 5
	jsr UpdateBars
;.endrepeat
bl:
;jmp bl
	rts
.endproc

.bss
BarRunning:
.repeat 16
.byte 0
.endrepeat

.segment "ZEROPAGE"
BarPtr:
.word 0
BarHeight:
.word 0
BarSpeed:
.word 0
BarTarget:
.word 0
ATemp:
.byte 0
YTemp:
.byte 0

delay:
.byte 0
NumBarsRunning:
.byte 0
debugindex:
.byte 0

.segment "PARTYCOL"
BarColor:

.segment "PARTYNAME"
PartyNames:

.macro AddParty name, color
.segment "PARTYNAME"
scrcode name
.segment "PARTYCOL"
.byte color
.segment "EMPTY"
.endmacro

AddParty "PS ", COLOR::BLUE
AddParty "KOK", COLOR::BLUE_LIGHT
AddParty "SDP", COLOR::RED_LIGHT
AddParty "VIH", COLOR::GREEN
AddParty "KES", COLOR::GREEN_LIGHT
AddParty "VAS", COLOR::RED
AddParty "RKP", COLOR::BLUE_LIGHT
AddParty "ISO", COLOR::BROWN
AddParty "NYT", COLOR::VIOLET
AddParty "SIN", COLOR::BLUE
AddParty "FAG", COLOR::RED
AddParty "FAC", COLOR::BLUE_LIGHT
AddParty "KRP", COLOR::BLUE
AddParty "JML", COLOR::BLACK
AddParty "WPK", COLOR::RED
AddParty "PIR", COLOR::VIOLET

.rodata
PartialChar:
;     0    2    4    6       8
.byte $20, $65, $61, $E7 ; , $A0


;KES 49 => 25
;PER 38 => 19
;KOK 37 => 19
;SDP 34 => 17
;VIH 15 => 8
;VAS 12 => 6
;RKP 9  => 5
;KD  5  => 3

BarHeightStart:
;       PIR, WPK, JML, KRP, FAC, FAG, SIN, NYT, ISO, RKP, VAS, KES, VIH, SDP, KOK,  PS
.byte     4,   4,   8,   8,  17,  22,   4,   4,  13,  17,  35,  53,  67,  76,  76,  94
