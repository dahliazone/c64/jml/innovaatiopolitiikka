.include "macros_h.s"
.include "imagemacros_h.s"

.import __SLIDEBM_RUN__, __SLIDESCR_RUN__, __SLIDECOL_RUN__

.export CopyColorRam
.export FirstSlide
.export ShowSlideNext

.export InitSlide, RunSlide

;From first.s
.import SlideBitmaps, SlideScreens, SlideColors

.segment "SIPILA"
GetBitmap "draft/sipitaulu.ocp"
.repeat 192 ; Pad to $6000
.byte 0
.endrepeat
GetScreen "draft/sipitaulu.ocp"

.segment "COLRAMSTORE"
cram1:
GetColRam "draft/sipitaulu.ocp"

.macro SelectBitmapMode
	lda #0
	sta $D021
	lda #%00000010 ; $4000- VIC block. Where Sipilä resides.
	sta $DD00
	lda #%00111011 ; Select bitmap mode.
	sta $D011
	lda #%10000000 ; Bitmap at $0000, screen at $2000 VIC BANK RELATIVE!
	sta $D018
	lda #%11011000 ; Multicolor mode
	sta $D016
.endmacro

.code

.proc CopyColorRam
	memcpy1000 $D800, cram1
	rts
.endproc

.proc FirstSlide
	SetPointer SlidePtr, __SLIDEBM_RUN__
	SetPointer SlideScreenPtr, __SLIDESCR_RUN__
	SetPointer SlideColPtr, __SLIDECOL_RUN__
	rts
.endproc

;Show current slide & move to next one.
;NOTE; Updates colorram to cram1, it's assumed we have to call CopyColorRam after this anyway...
.proc ShowSlideNext
	SetPointer BitmapPtr, SlidePos
	;Blit bitmap.
	ldx #SlideH
nextrow:
	;lda #0
	ldy #SlideW*8-1
prev:
	lda (SlidePtr), y
	sta (BitmapPtr), y
	dey
	bpl prev
	
	AddToPtr BitmapPtr, (8*40) ; How many bytes to add to move to same location on the next row on bitmap.
	AddToPtr SlidePtr, (8*8) ; SlideW*8 - How many bytes to add to move to same location on the next row on slide.
	dex
	bne nextrow
	;Bitmap done. Copy screen & color ram.
	SetPointer ScreenPtr, SlideScreenPos
	SetPointer ColPtr,	cram1+SlideScreenOff
	
	ldx #SlideH
nextrow2:
	ldy #SlideW-1
prev2:
	lda (SlideScreenPtr), y
	sta (ScreenPtr), y
	lda (SlideColPtr), y
	sta (ColPtr), y
	dey
	bpl prev2

	AddToPtr ScreenPtr, 40 ; ScreenW
	AddToPtr ColPtr, 40 ; ScreenW
	AddToPtr SlideScreenPtr, 8 ; SlideW
	AddToPtr SlideColPtr, 8 ; SlideW

	dex
	bne nextrow2

	rts
.endproc

.proc InitSlide
	jsr ShowSlideNext
	jsr CopyColorRam
	SelectBitmapMode
	rts
.endproc

.proc RunSlide
	;Nothing to do currently...
	rts
.endproc

.segment "ZEROPAGE"
;Bitmap
BitmapPtr:
.word 0
SlidePtr:
.word 0
;Screen
ScreenPtr:
.word 0
SlideScreenPtr:
.word 0
;Color
ColPtr:
.word 0
SlideColPtr:
.word 0
