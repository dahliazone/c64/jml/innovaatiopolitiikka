.macro AddEffect efinit, efrun, efduration
.segment "EFINITLO"
.byte <efinit
.segment "EFINITHI"
.byte >efinit
.segment "EFRUNLO"
.byte <efrun
.segment "EFRUNHI"
.byte >efrun
.segment "EFDELAY"
.byte efduration

.segment "EMPTY"
.endmacro
