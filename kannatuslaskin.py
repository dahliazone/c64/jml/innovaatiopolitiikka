#!/usr/bin/env python3
import sys, math

###############################################
# DATA
###############################################
# - kohdetiedosto
targetFile = 'bars.h.s'

# - puolueet ja niiden värit
puolueet = [
	{ 'name': 'PS',  'color': 'BLUE' },
	{ 'name': 'KOK', 'color': 'BLUE_LIGHT' },
	{ 'name': 'SDP', 'color': 'RED_LIGHT' },
	{ 'name': 'VIH', 'color': 'GREEN' },
	{ 'name': 'KES', 'color': 'GREEN_LIGHT' },
	{ 'name': 'VAS', 'color': 'RED' },
	{ 'name': 'RKP', 'color': 'BLUE_LIGHT' },
	{ 'name': 'ISO',  'color': 'BROWN' },
	{ 'name': 'NYT', 'color': 'VIOLET' },
	{ 'name': 'SIN', 'color': 'BLUE' },
	{ 'name': 'FAG', 'color': 'RED' },
	{ 'name': 'FAC', 'color': 'BLUE_LIGHT' },
	{ 'name': 'KRP', 'color': 'BLUE' },
	{ 'name': 'JML', 'color': 'BLACK' },
	{ 'name': 'WPK', 'color': 'RED' },
	{ 'name': 'PIR', 'color': 'VIOLET' },
]

# - normalisointi, summa enintään tämän verran lopputuloksessa
normalisointi = 512

# - ajastus eli montako framee per animaatio ja suhteelliset luvut
#   eka rivi aloitusarvot
ajastus = [
#                                                         PS , KOK, SDP, VIH, KES, VAS, RKP, ISO, NYT, SIN, FAG, FAC, KRP, JML, WPK, PIR
	{ 'name': 'aloitus',   'suhteellinen': [  21,  17,  17,  15,  12,   8,   4,   3,   1,   1,   5,   4,   2,   2,   1,   1 ] },
	{ 'name': 'kakkara 2', 'suhteellinen': [  18,  14,  14,  12,   8,   7,   3,   6,   2,   2,  10,   9,   4,  10,   2,   4 ] },
	{ 'name': 'arvot',     'suhteellinen': [  15,  11,  11,   9,   5,   6,   3,  12,   2,   2,  10,   9,   4,  15,   2,   4 ] },
	{ 'name': 'lonk',      'suhteellinen': [  11,   8,   8,   7,   4,   6,   3,  14,   2,   2,  12,   9,   4,  20,   2,   4 ] },
	{ 'name': 'aktiivi',   'suhteellinen': [   7,   7,   7,   7,   3,   6,   3,  18,   2,   2,  12,   9,   4,  20,   2,   4 ] },
	{ 'name': 'sotenuija', 'suhteellinen': [   7,   7,   7,   7,   2,   6,   3,  30,   2,   2,  12,   9,   4,  30,   2,   4 ] },
	{ 'name': 'sotefail',  'suhteellinen': [   7,   7,   7,   7,   1,   6,   3,  35,   2,   2,  12,   9,   4,  35,   2,   4 ] },
	{ 'name': 'talous',    'suhteellinen': [   7,   7,   7,   7,   0,   6,   3,  40,   2,   2,  12,   9,   4,  40,   2,   4 ] },
]


###############################################
# PÄÄ_OHJELMA
###############################################

print('Jumalauta kannatuslaskin v1.0')

voima = False
if (len(sys.argv) >= 2):
	for arg in sys.argv:
		if (arg == "-f" or arg == "--voima"):
			voima = True

print('Ylikirjoitetaan: ' + targetFile)

if (voima == False):
	userInput = input('Jatka? [Y/N] ')

	if (userInput.lower() in ('n', 'no')):
		print('operaatio peruttu')
		sys.exit(0)

# lasketaan prosentit
prosentit = []
for aikarivi in ajastus:
	total = sum(aikarivi['suhteellinen'])
	newRow = []
	for value in aikarivi['suhteellinen']:
		newRow.append(int(float(normalisointi) * float(value) / float(total)))
	prosentit.append(newRow)


# Kirjoitetaan kohdetiedosto
outputFile = open(targetFile, 'w')
#outputFile.write('testi\n')

outputFile.write('\n')
outputFile.write(';   ')
first = True
for p in reversed(puolueet):
	if first:
		first = False
		outputFile.write(p['name'].rjust(9))
	else:
		outputFile.write(', ' + p['name'].rjust(9))
outputFile.write('\n')

outputFile.write(';         ')
first = True
for p in reversed(puolueet):
	if first:
		first = False
		outputFile.write('SPD, TGT')
	else:
		outputFile.write(',  SPD, TGT')
outputFile.write(' ; Reaction to:\n')
outputFile.write('\n')

for i, part in enumerate(ajastus):
	if (i == 0):  # skipataan eka
		continue
	outputFile.write('AddBars   ')
	first = True
	prosIndex = i;
	j = len(prosentit[prosIndex]) - 1
	while j >= 0:
		value = prosentit[prosIndex][j]
		prevValue = prosentit[prosIndex - 1][j]

		# Lasketaan nopeus jolla liikutaan kohti
		speed = value - prevValue
		if (speed <= 0):
			speed = 255
		elif speed >= 0:
			speed = 1
		else:
			speed = 0

		target = abs(value - prevValue)
		#round( # math.floor#)

		if first:
			first = False
			outputFile.write(str(speed).rjust(3) + ', ' + str(target).rjust(3))
		else:
			outputFile.write(',  ' + str(speed).rjust(3) + ', ' + str(target).rjust(3))
		j -= 1
	outputFile.write(' ; ' + part['name'] + '\n')

outputFile.write('\n')
outputFile.close()

print('operaatio valmis\n\n')

# Lisäpäivitykset
print('Muista päivittää puolueet (jos tarve):')
print('==SNIP================================')
for p in puolueet:
	print('AddParty "' + p['name'].ljust(3) + '", COLOR::' + p['color'])
print('==SNIP================================\n')

print('Muista päivittää alkuarvot (jos tarve):')
print('==SNIP================================')
print(';       ', end='')
first = True
for p in reversed(puolueet):
	if first:
		first = False
		print(p['name'].rjust(3), end='')
	else:
		print(', ' + p['name'].rjust(3), end='')
print()

print('.byte   ', end='')
first = True
for value in reversed(prosentit[0]):
	if first:
		first = False
		print(str(value).rjust(3), end='')
	else:
		print(', ' + str(value).rjust(3), end='')
print()

print('==SNIP================================\n')
