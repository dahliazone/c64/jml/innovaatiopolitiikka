.include "macros_h.s"
.include "imagemacros_h.s"
.include "bars.h"
.include "effect.h"

.import InitSlide, RunSlide

.include "bars.h.s"

;Define here, what should be done, in which sequence.

AddEffect InitSlide, RunSlide, 200
AppendSlide "draft/sipitaulu.ocp" ; kakkara 2

AddEffect InitBars, RunBars, 190

AddEffect InitSlide, RunSlide, 190
AppendSlide "draft/sipitaulu2.ocp" ; arvot

AddEffect InitBars, RunBars, 200

AddEffect InitSlide, RunSlide, 190
AppendSlide "draft/sipitaulu-lonk.ocp"

AddEffect InitBars, RunBars, 190

AddEffect InitSlide, RunSlide, 190
AppendSlide "draft/sipitaulu-akt.ocp"

AddEffect InitBars, RunBars, 180

AddEffect InitSlide, RunSlide, 200
AppendSlide "draft/sipitaulu-sotenuija.ocp"

AddEffect InitBars, RunBars, 200

AddEffect InitSlide, RunSlide, 190
AppendSlide "draft/sipitaulu-sotefail.ocp"

AddEffect InitBars, RunBars, 190

AddEffect InitSlide, RunSlide, 200
AppendSlide "draft/sipitaulu3.ocp" ; talous

AddEffect InitBars, RunBars, 190

AddEffect InitSlide, RunSlide, 190
AppendSlide "draft/sipitaulu-aan.ocp"

;Demo ends at this screen.
AddEffect InitSlide, RunSlide, 200
AppendSlide "draft/sipitaulu-creds.ocp"