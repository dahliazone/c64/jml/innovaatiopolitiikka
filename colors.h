.IFDEF _INCLUDE_COLORS_H
.ELSE
_INCLUDE_COLORS_H=1

.scope COLOR
	BLACK      =$00
	WHITE      =$01
	RED        =$02
	CYAN       =$03
	VIOLET     =$04
	GREEN      =$05
	BLUE       =$06
	YELLOW     =$07
	
	MC_BLACK   =$08
	MC_WHITE   =$09
	MC_RED     =$0A
	MC_CYAN    =$0B
	MC_VIOLET  =$0C
	MC_GREEN   =$0D
	MC_BLUE    =$0E
	MC_YELLOW  =$0F
	;Rest not usable for hires chars in multicolor mode.
	ORANGE     =$08
	BROWN      =$09
	RED_LIGHT  =$0A
	GREY_DARK  =$0B
	GREY       =$0C
	GREEN_LIGHT=$0D
	BLUE_LIGHT =$0E
	GREY_LIGHT =$0F
.endscope
.ENDIF
