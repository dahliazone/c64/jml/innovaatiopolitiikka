.IFDEF _INCLUDE_IMAGEMACROS_H
.ELSE

headerlen=2
;http://codebase64.org/doku.php?id=base:c64_grafix_files_specs_list_v0.03
screenoffset=$1F40
colramoffset=$2338

;in "chars"
SlideW=8
SlideH=13
SlideX=28
SlideY=4
SlideOff=SlideY*40*8+SlideX*8
SlidePos=$4000+SlideOff
SlideScreenOff=SlideY*40+SlideX
SlideScreenPos=$6000+SlideScreenOff

;Macros for extracting relevant data from Advanced Art Studio files.
.macro GetBitmap fn
.incbin fn, headerlen, 8000
.endmacro

.macro GetScreen fn
.incbin fn, screenoffset+headerlen, 1000
.endmacro

.macro GetColRam fn
.incbin fn, colramoffset+headerlen, 1000
.endmacro

.macro GetSlideBitmap fn
.repeat SlideH, ypos
.incbin fn, headerlen+SlideOff+ypos*40*8, SlideW*8
.endrepeat
.endmacro

.macro GetSlideScreen fn
.repeat SlideH, ypos
.incbin fn, headerlen+screenoffset+SlideY*40+SlideX+ypos*40, SlideW
.endrepeat
.endmacro

.macro GetSlideCRam fn
.repeat SlideH, ypos
.incbin fn, headerlen+colramoffset+SlideY*40+SlideX+ypos*40, SlideW
.endrepeat
.endmacro

.macro AppendSlide fn
.segment "SLIDEBM"
GetSlideBitmap fn
.segment "SLIDESCR"
GetSlideScreen fn
.segment "SLIDECOL"
GetSlideCRam fn
.segment "EMPTY"
.endmacro

.ENDIF
