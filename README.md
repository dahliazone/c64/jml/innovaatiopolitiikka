# Innovaatiopolitiikka by Jumalauta (Source code)

## Prequisities

* cc65 compiler package: https://cc65.github.io/
  * e.g. Debian based: sudo apt install cc65
  * e.g. OS-X: brew install cc65
* make
* VICE emulator: https://vice-emu.sourceforge.io/
  * Have VICE emulator binary `x64sc` in path or edit makefile variable `X64`
  * Have VICE bundled binary `c1541` in path or edit makefile variable `C1541`
* Tested with Linux & OS X (might work in Windows too with some minor tweaks)


## Compiling & running

### Build (compile & link prg file and generate the disk image):
```
make
```


### Run (and compile):
```
make run
```

Note that enabling warp mode will make the emulator start a lot faster but test the program also
without the Warp mode as sometimes there are issues with disk drive emulation.


### Clean build artifacts:
```
make clean
```


## License

See LICENSE file for copyright and license (MIT) information.


## Links to this prod

* DemoZOO: https://demozoo.org/productions/267871/
* Pouet: https://www.pouet.net/prod.php?which=83018
* CSDB: https://csdb.dk/release/?id=181986

